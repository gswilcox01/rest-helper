from setuptools import setup, find_packages

install_requires = [
    'click',
    'keyring',
    'requests'
    ]

test_requires = [
    'mock',
    'nose'
    ]

config = {
    'name': 'rest_helper',
    'version': '0.1',
    'description': 'Simple CLI for helping call REST endpoints',
    'author': 'Gary Wilcox',
    'env-url': 'none',
    'download_url': 'none',
    'author_email': 'gary@dugan-wilcox.com',
    'packages': find_packages(),
    'install_requires': install_requires,
    'tests_require': test_requires,
    'scripts': [],
    'entry_points': {
        'console_scripts': ['rc=resthelper.command_line:cli'],
    }
}

setup(**config)