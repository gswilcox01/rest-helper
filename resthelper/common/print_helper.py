def left_format(_list, field, header_len):
    max_len = max(len(str(obj[field])) for obj in _list)
    max_len = max(max_len, header_len) 
    this_format = '{:<' + str(max_len + 3) + '}'
    return this_format
    
def print_formatted_table(header, fields, _list):
    # Create line_format based on MAX WIDTH of each field we are printing
    line_format = ""
    for idx, field in enumerate(fields):
        header_len = len(header[idx])
        line_format += left_format(_list, field, header_len)
    
    # print header & then each row in the list
    print line_format.format(*header)
    for row in _list:
        # first convert into a list of values (instead of a dict/object)
        new_row = []
        for field in fields:
            new_row.append(row[field])
        # then print it
        print line_format.format(*new_row)

def test_formatted_table():
    header = ['IDENTIFIER:', 'NAME:']
    fields = ['id', 'name']
    _list = [ 
        { 'id' : 1, 'name' : 'gary' },
        { 'id' : 2, 'name' : 'bob maloney' }
    ]
    print_formatted_table(header, fields, _list)

if __name__ == '__main__':
    test_formatted_table()