import json
import os
from resthelper.common import print_helper
import getpass
import click
import keyring
import sys

CONFIG_FOLDER='.rc'
APP_FOLDER='apps'
NEW_APPNAME='new'

def abs_config_dir():
    home = os.path.expanduser("~")
    return os.path.join(home, CONFIG_FOLDER)

def abs_main_filename():
    return os.path.join(abs_config_dir(), ".main")

def abs_default_filename():
    return os.path.join(abs_config_dir(), ".default")

def abs_current_app_filename():
    main = read_config(abs_main_filename())
    return abs_app_filename(main['active-app'])

def abs_app_dir():
    return os.path.join(abs_config_dir(), APP_FOLDER)

def abs_app_filename(app_name):
    filename = "." + app_name
    return os.path.join(abs_app_dir(), filename)
    
def init():
    app_dir = abs_app_dir()
    if not os.path.exists(app_dir):
        os.makedirs(app_dir)
    
    main_fn = abs_main_filename()
    if not os.path.isfile(main_fn):
        config = { 
            'active-app' : 'new' 
        }
        write_config(main_fn, config)

    default_fn = abs_default_filename()
    if not os.path.isfile(default_fn):
        config = { 
            'content' : 'application/json',
            'security' : 'none',
            'use-osuser': True,
            'use-common-password': True,  # store passwords @ username, instead of @ APP+ENV+username
            'use-keyring': True           # store passwords in the OS keyring
        }
        write_config(default_fn, config)
    
def read_main_config():
    return read_config(abs_main_filename())

def read_default_config():
    return read_config(abs_default_filename())

def read_current_app():
    main = read_config(abs_main_filename())
    return read_config(abs_app_filename(main['active-app']))
    
def read_current_combined():
    # READ the different configs first
    defaults = read_config(abs_default_filename())
    app, env = read_current_app_and_env()
    
    # THEN merge them together
    context = defaults.copy()
    context.update(app)
    context.update(env)
    
    # THEN remove the extra ENV and RESOURCES
    context.pop('envs', None)
    context.pop('resources', None)
    return context

def prompt_for_username(config, ask_for_password=False):
    # if none, just return
    # if basic, basic-username, basic-password
    # if oauth1, oa1-key, oa1-secret
    if config['security'] == 'none':
        return config
    elif config['security'] == 'basic':
        prompt_for_basic_creds(config, ask_for_password)
        return config
    else:
        print "ERROR: Only 'none' and 'basic' security has been implemented.  Try again later..."
        sys.exit()

def prompt_for_basic_creds(config, ask_for_password=False):
    prompted_for_username = False
    if 'username' not in config:
        # for now if USERNAME is not set in configuration always prompt
        # AND always default to current OS username
        # FUTURE - only default to OS username if use-osuser=true
        prompted_for_username = True
        os_user = getpass.getuser()
        config['username'] = click.prompt('Username', default=os_user)
    # for now use a COMMON PASSWORD, for this USERNAME (i.e. assume it's a LANID)
    # AND always check the os_keyring to see if it's been saved
    # FUTURE - only if use-common-password=true
    #        - otherwise store passwords @ specific "username.app.env" level
#         KEYRING_SERVICE_NAME = 'RESTCLI.{}.{}'.format(config['app-name'], config['env-name'])
    KEYRING_SERVICE_NAME = 'RESTCLI'
    password = keyring.get_password(KEYRING_SERVICE_NAME, config['username'])
    if ask_for_password or password is None:
        if not prompted_for_username:
            print "Username: {}".format(config['username'])
        password = getpass.getpass()
        
    # ALWAYS, set the password in the OS keyring (so it's used on next invocation)
    # FUTURE - only if use-keyring=true
    keyring.set_password(KEYRING_SERVICE_NAME, config['username'], password)
    config['password'] = password
        

def read_current_app_and_env():
    config = read_current_app()
    active_name = config['active-env']
    active_env = {}
    for env in config['envs']:
        if env['env-name'] == active_name:
            active_env = env
    return config, active_env
    
def read_config(fn):
    config = {}
    if os.path.isfile(fn):
        with open(fn) as f:
            config = json.load(f)
    return config
        
def write_activate(config):
    # WRITE OUT CONFIG!
    config_fn = abs_app_filename(config['app-name'])
    write_config(config_fn, config)
    activate_app(config['app-name'])
    
def write_config(fn, config):
    with open(fn, "w") as f:
        json.dump(config, f, indent=4)

def activate_app(app_name):
    if app_name.isdigit():
        app_number = int(app_name)
        app_name = lookup_app_name(app_number)
    main = read_config(abs_main_filename())
    main['active-app'] = app_name
    write_config(abs_main_filename(), main)
#     print_active_config(app_name)

def lookup_resource_alias(name):
    config, env = read_current_app_and_env()
    for resource in config.get('resources',[]):
        if resource['alias'] == name:
            return resource
    return None
    
def lookup_app_name(app_number):
    app_idx = app_number - 1
    for idx, app_name in enumerate(app_names()):
        if idx == app_idx:
            return app_name

def app_names():
    app_dir = abs_app_dir()
    app_names_list = (file[1:] for file in os.listdir(app_dir) 
         if os.path.isfile(os.path.join(app_dir, file)) and file.startswith(".")
    )
    return app_names_list
    
def print_config():
    main_fn = abs_main_filename()
    main = read_config(main_fn)
    default_fn = abs_default_filename()
    default = read_config(default_fn)
    print "MAIN CONFIG: ({})".format(main_fn)
    print json.dumps(main, indent=4)
    print "DEFAULT CONFIG: ({})".format(default_fn)
    print json.dumps(default, indent=4)
    print_active_config(main['active-app'])
    
def print_active_config(app_name):
    app_fn = abs_app_filename(app_name)
    config = read_config(app_fn)
    print "ACTIVE APP ({}) CONFIG: ({})".format(app_name, app_fn)
    print json.dumps(config, indent=4)
    
def set_config_value(name, value, is_default=False, is_env=False):
    if is_default:
        config = read_config(abs_default_filename())
        config[name] = value
        print "Adding the following to DEFAULT config"
        print "{}={}".format(name, value)
        write_config(abs_default_filename(), config)
    elif is_env:
        config, env = read_current_app_and_env()
        env[name] = value
        print "Adding the following to CURRENT APP ENVIRONMENT config"
        print "{}={}".format(name, value)
        write_config(abs_current_app_filename(), config)
    else:
        config = read_current_app()
        config[name] = value
        print "Adding the following to CURRENT APP config"
        print "{}={}".format(name, value)
        write_config(abs_current_app_filename(), config)
    
def get_config_value(name):
    init()
    # ALWAYS read from active-app!
    main = read_config(abs_main_filename())
    active_fn = abs_app_filename(main['active-app'])
    active = read_config(active_fn)
    return active[name]

if __name__ == '__main__':
    init()
    print_config()
