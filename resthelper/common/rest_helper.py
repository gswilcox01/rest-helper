from requests.auth import HTTPBasicAuth
import requests
import sys

# move to REST_HELPER
def get_url(url, config):
    VERIFY=True
    
    print "GETTING : " + url
    
    if config.get('security', 'none') == 'basic':
        username = config['username']
        password = config['password']
        response  = requests.get(url, auth=HTTPBasicAuth(username, password), verify=VERIFY)
    else:
        response  = requests.get(url, verify=VERIFY)
    
    check_status_exit(response)
    return response
    
# move to REST_HELPER
def check_status_exit(r):
    if r.status_code not in (200, 204):
        print "ERROR: {} Status Code".format(r.status_code)
        sys.exit()

if __name__ == '__main__':
    pass
