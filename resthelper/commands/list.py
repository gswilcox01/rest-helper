import click
import os
from resthelper.common import config_helper, print_helper, rest_helper
import json
from requests.auth import HTTPBasicAuth
import requests
import sys

@click.command()
@click.option('--debug', is_flag=True, help="Print combined env, app, default config")
@click.option('--password', is_flag=True, help="Prompt for password, even if cached already")
@click.argument('resource', required=False)
def list(debug, password, resource):
    """ 
    Command to LIST a resource.
    
    The RESOURCE argument must be a valid resource alias you have
    configured.  With a "fields" attribute manually added to the . file.
    
    Example:
    "fields": ["resourceId", "name", "organizationName", "status"]
    """
    if debug:
        debug_config(password)
    else:
        list_resource(resource, password)

def debug_config(ask_for_password):
    config = config_helper.read_current_combined()
    config = config_helper.prompt_for_username(config, ask_for_password)
    print "COMBINED env, app, default CONFIG:"
    print json.dumps(config, indent=4)

def list_resource(name, ask_for_password):
    config = config_helper.read_current_combined()
    config = config_helper.prompt_for_username(config, ask_for_password)

    # LOOKUP RESOURCE ALIAS for name
    resource = config_helper.lookup_resource_alias(name)
    if resource is None:
        "ERROR: 'list' command requires a resource alias to be defined"
        "ERROR: NAME argument ({}) does not exist as a resource alias!".format(name)
        sys.exit()
        
    fields = resource.get('fields', None)
    if fields == None or len(fields) < 1:
        "ERROR: 'list' command requires a resource alias with 'fields' attribute"
        "ERROR: 'fields' attribute was missing"
        sys.exit()

    # Create the url
    url = config['env-url']
    path = resource['path']
    if not path.startswith('/'): url += '/'
    url += path

    # Call the url and parse results
    response = rest_helper.get_url(url, config)
    response_parsed = json.loads(response.text)
    
    # Print as a table
    headers = [field.upper() + ":" for field in fields]
    print_helper.print_formatted_table(headers, fields, response_parsed)
    
    
if __name__ == '__main__':
    debug_config('gary')
