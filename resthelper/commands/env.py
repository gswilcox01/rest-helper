import click
import os
from resthelper.common import config_helper, print_helper

@click.command()
@click.option('--new', is_flag=True, help="Initialize a new env for THIS app")
@click.argument('name', required=False)
def env(new, name):
    """ Configure, list or switch environments for an app"""
    if new:
        init_env(name)
    elif name is None:
        print_envs()
    else:
        activate_env(name)

def init_env(name):
    config = config_helper.read_current_app()
    print "Initializing a new environment for current app ({}):".format(config['app-name'])
    
    env = {}
    env['env-name'] = click.prompt('Please enter an env name', default='dev')
    env['env-url'] = click.prompt('Please enter a BASE URL for env', default='http://localhost:8080')
    config['active-env'] = env['env-name']
    config['envs'].append(env)
    config_helper.write_activate(config)
    print_envs()

def activate_env(name):
    config = config_helper.read_current_app()
    
    if name.isdigit():
        number = int(name)
        env_idx = number - 1
        for idx, env in enumerate(config['envs']):
            if idx == env_idx:
                config['active-env'] = env['env-name']
    else:
        config['active-env'] = name
        
    config_helper.write_activate(config)
    print_envs()
        
def print_envs():
    # determine active env
    config = config_helper.read_current_app()
    active_env = config['active-env']
    
    # create list of ALL envs
    # with calculated fields we want to display 
    _list = []
    for idx, env in enumerate(config['envs']):
        number = str(idx + 1)
        if env['env-name'] == active_env:
            number += "*" 
        display = {
            'number' : number,
            'app-name' : config['app-name'],
            'env-name' : env['env-name'],
            'env-url' : env['env-url']
        }
        _list.append(display)
    
    # now display with print_helper
    header = ['NUM:', 'APP:', 'ENV-NAME:', 'ENV-URL:']
    fields = ['number', 'app-name', 'env-name', 'env-url']
    print_helper.print_formatted_table(header, fields, _list)

if __name__ == '__main__':
    pass
