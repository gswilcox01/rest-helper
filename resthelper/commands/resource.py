import click
import os
from resthelper.common import config_helper, print_helper

@click.command()
@click.option('--new', is_flag=True, help="Initialize a new resource for THIS app")
def resource(new):
    """ Configure or list resource aliases for an app"""
    if new:
        init_resource()
    else:
        print_resources()
    
def init_resource():
    config = config_helper.read_current_app()
    print "Initializing a new resource alias for current app ({}):".format(config['app-name'])
    
    res = {}
    res['alias'] = click.prompt('Please enter an ALIAS/shortname for the resource', default='consumer')
    res['path'] = click.prompt('Please enter the REAL PATH for the resource', default='v1/consumers')
    if 'resources' not in config:
        config['resources'] = []
    
    config['resources'].append(res)
    config_helper.write_activate(config)
    config_helper.print_active_config(config['app-name'])

def print_resources():
    # read current app
    config = config_helper.read_current_app()
    if 'resources' not in config:
        print "None"
        return
    
    # create list of ALL resources for this app
    # with calculated fields we want to display 
    _list = []
    for idx, res in enumerate(config.get('resources',[])):
        number = str(idx + 1)
        field_count = len(res.get('fields',[]))
        display = {
            'number' : number,
            'app-name' : config['app-name'],
            'alias' : res['alias'],
            'path' : res['path'],
            'field_count' : field_count
        }
        _list.append(display)
    
    # now display with print_helper
    header = ['NUM:', 'APP:', 'ALIAS:', 'REAL-PATH:', 'FIELDS:']
    fields = ['number', 'app-name', 'alias', 'path', 'field_count']
    print_helper.print_formatted_table(header, fields, _list)

if __name__ == '__main__':
    pass
