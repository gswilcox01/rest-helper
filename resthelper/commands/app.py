import click
import os
import json
import getpass
from resthelper.common import config_helper, print_helper
from resthelper.commands import env, resource

@click.command()
@click.option('--new', is_flag=True, help="Initialize a new app")
@click.option('--all', is_flag=True, help="Print app, env, and resource tables for current app")
@click.option('--clear', is_flag=True, help="Clear the current active app / start fresh")
@click.option('--saveas', is_flag=True, help="Save the current active app as a new name")
@click.argument('appname', required=False)
def app(new, all, clear, saveas, appname):
    """ Configure, list or switch apps"""
    if new:
        init_app(appname)
    elif clear:
        new_app()
    elif saveas:
        save_active_as(appname)
    elif all:
        config = config_helper.read_current_app()
        active_app = config['app-name']
        print "APPS:"
        print_apps()
        print "\nENVS (for {}):".format(active_app)
        env.print_envs()
        print "\nRESOURCES (for {}):".format(active_app)
        resource.print_resources()
    elif appname is None:
        print_apps()
    else:
        config_helper.activate_app(appname)
        print_apps()

def new_app():
    config = {}
    config['app-name'] = 'new'
    config['security'] = 'none'
    env = {}
    env['env-name'] = 'default'
    env['env-url'] = 'http://localhost:8080'
    config['active-env'] = env['env-name']
    config['envs'] = [env]
    config_helper.write_activate(config)
    config_helper.print_active_config(config['app-name'])
    
def init_app(appname):
    print "Initializing a new application:"
    
    config = {}
    env = {}
    if appname is None:
        config['app-name'] = click.prompt("Please enter a short NAME for this app")
    else:
        config['app-name'] = appname
        print "APP_NAME = " + appname
        
    env['env-name'] = click.prompt('Please enter the default env name', default='localhost')
    env['env-url'] = click.prompt('Please enter the default env URL', default='http://localhost:8080')
    config['active-env'] = env['env-name']
    config['envs'] = [env]
    
    options = [
        {'code': 'none', 'display': 'None'},
        {'code': 'basic', 'display': 'Basic Auth'},
        {'code': 'oauth1', 'display': 'OAuth 1'},
    ]
    idx = prompt_choice("Security Options:", options, 'Please enter a security option for app')
    config['security'] = options[idx]['code']
    
    config_helper.write_activate(config)
    config_helper.print_active_config(config['app-name'])

    
def save_active_as(appname):
    # READ current active
    main_fn = config_helper.abs_main_filename()
    main = config_helper.read_config(main_fn)
    active_fn = config_helper.abs_app_filename(main['active-app'])
    active = config_helper.read_config(active_fn)
    
    # RENAME and save
    active['app-name'] = appname
    new_fn = config_helper.abs_app_filename(appname)
    config_helper.write_config(new_fn, active)
    config_helper.activate_app(appname)
    
def print_apps():
    # determine active app
    active_config = config_helper.read_current_app()
    active_app = active_config['app-name']
    
    # create list of ALL apps in app_dir
    # with calculated fields we want to display 
    app_list = []
    for idx, app_name in enumerate(config_helper.app_names()):
        app_fn = config_helper.abs_app_filename(app_name)
        config = config_helper.read_config(app_fn)
        active_env_name = config['active-env']
        active_env = {}
        for env in config['envs']:
            if env['env-name'] == active_env_name:
                active_env = env
        active_env_url = active_env['env-url']
        number = str(idx + 1)
        if app_name == active_app:
            number += "*" 
        app = {
            'number' : number,
            'app-name' : app_name,
            'security' : config['security'],
            'env' : active_env_name,
            'env-url' : active_env_url
        }
        app_list.append(app)
    
    # now display with print_helper
    header = ['NUM:', 'NAME:', 'SECURITY:', 'ENV:', 'ENV-URL']
    fields = ['number', 'app-name', 'security', 'env', 'env-url']
    print_helper.print_formatted_table(header, fields, app_list)
    
def prompt_choice(header, options, question, default=0):
    print header
    for idx, option in enumerate(options):
        print "{} : {}".format(idx, option['display'])
    idx = click.prompt(question, default=default)
    return idx
    
if __name__ == '__main__':
    pass
