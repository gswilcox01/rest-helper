import click
import os
from resthelper.common import config_helper, print_helper, rest_helper
import json
from requests.auth import HTTPBasicAuth
import requests
import sys

@click.command()
@click.option('--debug', is_flag=True, help="Print combined env, app, default config")
@click.option('--password', is_flag=True, help="Prompt for password, even if cached already")
@click.argument('name', required=False)
@click.argument('id', required=False)
def get(debug, password, name, id):
    """ Command to GET a resource"""
    if debug:
        debug_config(password)
    else:
        get_resource(name, id, password)

def debug_config(ask_for_password):
    config = config_helper.read_current_combined()
    config = config_helper.prompt_for_username(config, ask_for_password)
    print "COMBINED env, app, default CONFIG:"
    print json.dumps(config, indent=4)

def get_resource(name, id, ask_for_password):
    config = config_helper.read_current_combined()
    config = config_helper.prompt_for_username(config, ask_for_password)
    
    if name is None:
        name = "/"
        
    url = ''
    if name.startswith('http'):
        url = name
    else:
        # BASE URL
        url = config['env-url']
        
        # LOOKUP RESOURCE ALIAS for name
        resource = config_helper.lookup_resource_alias(name)
        if resource is not None:
            path = resource['path']
            if not path.startswith('/'): url += '/'
            url += path
        else:
            # OTHERWISE consider the name to be the path
            if not name.startswith('/'): url += '/'
            url += name
        
        # And if an ID was passed in, append it to the end of the path
        if id is not None:
            url += '/' + str(id)
    
    response = rest_helper.get_url(url, config)
    # for now assume everything is application/json
    response_dict = json.loads(response.text)
    print json.dumps(response_dict, indent=4)
    
if __name__ == '__main__':
    debug_config('gary')
