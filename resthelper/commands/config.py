import click
import os
import getpass
from resthelper.common import config_helper
import sys

@click.command()
@click.option('--default', is_flag=True, help="Set default config value")
@click.option('--env', is_flag=True, help="Set current env config value")
@click.option('--pick', is_flag=True, help="Pick a config value from a list")
@click.argument('name', required=False)
@click.argument('value', required=False)
def config(default, env, pick, name, value):
    """ Configure NVP for app or env"""
    #
    # process pickers first!
    #
    if pick:
        name, value = pick_one(name)

    #    
    # then other options
    #
    if name is None:
        config_helper.print_config()
    elif value is None:
        print "ERROR: Both name and value are required"
        sys.exit()
    elif default:
        # store in DEFAULT config
        config_helper.set_config_value(name, value, True)
    elif env:
        # store in ENV config
        config_helper.set_config_value(name, value, False, True)
    else:
        # store in APP config
        config_helper.set_config_value(name, value)

def pick_one(name):
    if name is None:
        name = pick_picker();
        
    if name == 'content':
        return name, pick_content()
    elif name == 'security':
        return name, pick_security()
    else:
        print "ERROR: There no picker defined for : {}".format(name)
        sys.exit()

def pick_picker():
    options = [
        {'code': 'content', 'display': 'Content Type Header (content)'},
        {'code': 'security', 'display': 'Security Type (security)'}
    ]
    idx = prompt_choice("Configuration Pickers available:", options, 'Please choose a configuration picker')
    return options[idx]['code']

def pick_content():
    options = [
        {'code': 'text/plain', 'display': 'text/plain'},
        {'code': 'application/json', 'display': 'application/json'},
        {'code': 'application/xml', 'display': 'application/xml'}
    ]
    idx = prompt_choice("Content-Type Options:", options, 'Please choose a content-type', 1)
    return options[idx]['code']
    
def pick_security():
    options = [
        {'code': 'none', 'display': 'None'},
        {'code': 'basic', 'display': 'Basic Auth'},
        {'code': 'oauth1', 'display': 'OAuth 1'},
    ]
    idx = prompt_choice("Security Options:", options, 'Please choose a security option')
    return options[idx]['code']

def prompt_choice(header, options, question, default=0):
    print header
    for idx, option in enumerate(options):
        print "{} : {}".format(idx, option['display'])
    idx = click.prompt(question, default=default)
    return idx
    
if __name__ == '__main__':
    pass
