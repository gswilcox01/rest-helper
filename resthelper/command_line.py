import click
import os
import getpass
import keyring
import json
import sys
from resthelper import old_config_helper
from resthelper.common import config_helper

@click.group()
def cli():
    config_helper.init()
    pass

from commands import app
from commands import config
from commands import env
from commands import resource

cli.add_command(app.app)
cli.add_command(config.config)
cli.add_command(env.env)
cli.add_command(resource.resource)

from commands import get
from commands import list
cli.add_command(get.get)
cli.add_command(list.list)

if __name__ == '__main__':
    cli()
